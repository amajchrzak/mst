#include<iostream>
#include<vector>
#include<time.h>

using namespace std;

struct node;
struct mst;
vector<mst> primAlg(vector<node>& nodeVec, vector<mst>& setVec, vector<mst>& tree, int vertex, int edges);
void pickMinimumNodeValue(vector<node>& nodeVec, vector<mst>& setVec, vector<mst>& tree);
void changeDistance(vector<mst>& setVec, int vertex, vector<node>& nodeVec);
void changeParent(vector<mst>& setVec, int vertex, int min, vector<node>& nodeVec, vector<mst>& tree);

struct node
{
	int ver;			//number of vertices
	int distance;		//weight of the minimum edge
	bool isSetSummit;	//Is vertice used ?
	int parent;			//who is the parent of vertice
};

struct mst 
{
	int nodeA;			
	int nodeB;
	int weight;
};

//function choosing the minimum value of distances, then 
//setting parrent, check if vertice is used and changing
//distance beetwen nodes
void pickMinimumNodeValue(vector<node>& nodeVec, vector<mst>& setVec, vector<mst>& tree)
{
	int minimumNodeVal = -1;
	int min;
	int k = -1;

	min = 2147483647;
	for (int i = 0; i < nodeVec.size(); i++) { //przeszukanie pozostałych 9 liczb
		if (min > nodeVec[i].distance && nodeVec[i].isSetSummit == false) {
			min = nodeVec[i].distance;
			k = i;
		}
	}
	minimumNodeVal = k;
	changeParent(setVec, minimumNodeVal, min, nodeVec, tree);
	nodeVec[minimumNodeVal].isSetSummit = true;
	changeDistance(setVec, minimumNodeVal, nodeVec);
}

//function changing parent of nodes
void changeParent(vector<mst>& setVec, int vertex, int min, vector<node>& nodeVec, vector<mst>& tree)
{
	vector<mst> helpingVec;
	if (vertex != 0)
	{
		for (int i = 0; i < setVec.size(); i++)
		{
			if (setVec[i].weight == min && nodeVec[setVec[i].nodeA].isSetSummit == true && nodeVec[setVec[i].nodeB].isSetSummit == false)
			{
				helpingVec.push_back(setVec[i]);
			}
			else if (setVec[i].weight == min && nodeVec[setVec[i].nodeA].isSetSummit == false && nodeVec[setVec[i].nodeB].isSetSummit == true)
			{
				helpingVec.push_back(setVec[i]);
			}
		}
		for (int i = 0; i < helpingVec.size(); i++)
		{
			if (helpingVec[i].nodeA == vertex)
			{
				nodeVec[vertex].parent = helpingVec[i].nodeB;
				tree.push_back(helpingVec[i]);

			}
			if (helpingVec[i].nodeB == vertex)
			{
				nodeVec[vertex].parent = helpingVec[i].nodeA;
				tree.push_back(helpingVec[i]);
			}
		}
	}
}

//function changing distances beetwen nodes
void changeDistance(vector<mst>& setVec, int vertex, vector<node>& nodeVec)
{
	vector<mst> helpingVector;
	for (int i = 0; i < setVec.size(); i++)
	{
		if ((setVec[i].nodeA == vertex) || (setVec[i].nodeB == vertex))
		{
			helpingVector.push_back(setVec[i]);
		}
	}
	for (int i = 0; i < helpingVector.size(); i++)
	{
		if (nodeVec[helpingVector[i].nodeB].distance > helpingVector[i].weight && nodeVec[helpingVector[i].nodeB].isSetSummit == false)
		{
			nodeVec[helpingVector[i].nodeB].distance = helpingVector[i].weight;
		}
		else if (nodeVec[helpingVector[i].nodeA].distance > helpingVector[i].weight && nodeVec[helpingVector[i].nodeA].isSetSummit == false)
		{
			nodeVec[helpingVector[i].nodeA].distance = helpingVector[i].weight;
		}
	}
}
	
vector<mst> primAlg(vector<node>& nodeVec, vector<mst>& setVec, vector<mst>& tree,int vertex, int edges)
{
	int i = 0;
	while (true)
	{
		pickMinimumNodeValue(nodeVec, setVec, tree);
		i++;
		if(tree.size() == vertex - 1)
		{
			break;
		}
	}
	return tree;
}

int main() 
{
	/*int vertex = 6;
	int edges = 9;*/

	cout << "---PRIM ALGORITHM---" << endl;
	double suma = 0;
	for (int j = 0; j < 10; j++) {
		double start = clock();
		for (int i = 0; i < 10000; i++) {
			int vertex = 5;
			int edges = 8;

			vector<node> nodeVec(vertex);
			vector<mst> setVec(edges);
			vector<mst> mstSet;

			setVec[0] = { 0,1,1 };
			setVec[1] = { 0,2,2 };
			setVec[2] = { 0,3,2 };
			setVec[3] = { 2,3,3 };
			setVec[4] = { 3,4,3 };
			setVec[5] = { 3,1,4 };
			setVec[6] = { 1,4,5 };
			setVec[7] = { 0,4,6 };
			nodeVec[0] = { 0, 0, false, -1 };
			nodeVec[1] = { 1, 2147483647, false, -1 };
			nodeVec[2] = { 2, 2147483647, false, -1 };
			nodeVec[3] = { 3, 2147483647, false, -1 };
			nodeVec[4] = { 4, 2147483647, false, -1 };
			mstSet = primAlg(nodeVec, setVec, mstSet, vertex, edges);
			//for (const auto& x : mstSet)
			//{
			//	cout << "{" << x.nodeA << "," << x.nodeB << "}" << " w: " << x.weight << endl;
			//}
		}
		double stop = clock();
		double wynik = (double)(stop - start) / CLOCKS_PER_SEC;
		cout << j+1 << ". " << wynik << "s" <<endl;
		suma += wynik;
	}

	cout << "Srednia: " << suma/10;
	//set vectors//
	//nodeVec[0] = {0, 0, false, -1 };
	//nodeVec[1] = {1, 2147483647, false, -1 };
	//nodeVec[2] = {2, 2147483647, false, -1 };
	//nodeVec[3] = {3, 2147483647, false, -1 };
	//nodeVec[4] = {4, 2147483647, false, -1 };
	//nodeVec[5] = {5, 2147483647, false, -1 };
	//setVec[0] = { 0, 1, 4 };
	//setVec[1] = { 0, 2, 6 };
	//setVec[2] = { 2, 1, 6 };
	//setVec[3] = { 1, 4, 4 };
	//setVec[4] = { 2, 3, 1 };
	//setVec[5] = { 1, 3, 3 };
	//setVec[6] = { 4, 3, 2 };
	//setVec[7] = { 4, 5, 7 };
	//setVec[8] = { 3, 5, 3 };
	//-------//

	//for (const auto& x : mstSet)
	//{
	//	cout << "{" << x.nodeA << "," << x.nodeB << "}" << " w: " << x.weight << endl;
	//}
	return 0;
}