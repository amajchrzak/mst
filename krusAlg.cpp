
#include<iostream>
#include<algorithm>
#include<vector>
#include<time.h>

using namespace std;

struct node;
struct edge;
vector<edge> kruskalAlg(vector<node>& nodeVec, vector<edge>& edgeVec, int amountOfVertex, int amountOfEdges);
int findSet(int x, vector<node>& nodeVec);
void checkNode(int a, int b, vector<node>& nodeVec);
void sortByEdge(vector<edge>& edgeVec);



struct node
{
	int parent;
	int rank;
};

struct edge
{
	int nodeA;
	int nodeB;
	int weight;

};

bool operator<(const edge& c1, const edge& c2) { return c1.weight > c2.weight; }

vector<edge> kruskalAlg(vector<node>& nodeVec, vector<edge>& edgeVec, int amountOfVertex, int amountOfEdges)
{
	vector<edge> mst;
	int i = 0;
	int j = 0;
	int nodeA;
	int nodeB;
	sortByEdge(edgeVec);
	while (i < amountOfVertex && j < amountOfEdges)
	{
		nodeA = findSet(edgeVec[j].nodeA, nodeVec);
		nodeB = findSet(edgeVec[j].nodeB, nodeVec);

		if (nodeA == nodeB)
		{
			j++;
			continue;
		}
		checkNode(nodeA, nodeB, nodeVec);

		mst.push_back(edgeVec[j]);
		i++;
	}
	return mst;
}

int findSet(int x, vector<node>& nodeVec)
{
	if(nodeVec[x].parent == -1)
	{
		return x;
	}
	return nodeVec[x].parent = findSet(nodeVec[x].parent, nodeVec);
}

void checkNode(int a, int b, vector<node>& nodeVec)
{
	if(nodeVec[a].rank > nodeVec[b].rank)
	{
		nodeVec[b].parent = a;
	}
	else if(nodeVec[a].rank <  nodeVec[b].rank)
	{
		nodeVec[a].parent = b;
	}
	else
	{
		nodeVec[a].parent = b;
		nodeVec[b].rank += 1;
	}
}

void sortByEdge(vector<edge>& edgeVec)
{
	std::sort(edgeVec.begin(), edgeVec.end(),
		[](const edge& struct1, const edge& struct2)
		{
			return (struct1.weight < struct2.weight);
		});
}

int main()
{
	cout << "---KRUSKAL ALGORITHM---" << endl;
	double suma = 0.0;
	for (int j = 0; j < 10; j++) {
		double start = clock();
		for (int i = 0; i < 10000; i++) {
			int vertex = 5;
			int edges = 8;
			vector<edge> mst;
			vector<edge> edgeVec(8);
			vector<node> nodeVec(5);
			edgeVec[0] = { 0,1,1 };
			edgeVec[1] = { 0,2,2 };
			edgeVec[3] = { 0,3,2 };
			edgeVec[2] = { 2,3,3 };
			edgeVec[5] = { 3,4,3 };
			edgeVec[4] = { 3,1,4 };
			edgeVec[6] = { 1,4,5 };
			edgeVec[7] = { 0,4,6 };
			nodeVec[0] = { -1, 0 };
			nodeVec[1] = { -1, 0 };
			nodeVec[2] = { -1, 0 };
			nodeVec[3] = { -1, 0 };
			nodeVec[4] = { -1, 0 };
			mst = kruskalAlg(nodeVec, edgeVec, vertex, edges);
			//for (const auto& x : mst)
			//{
			//	cout << "{" << x.nodeA << "," << x.nodeB << "}" << " w: " << x.weight << endl;
			//}
		}
		double stop = clock();
		double wynik = (double)(stop - start) / CLOCKS_PER_SEC;
		cout << j + 1 << ". " << wynik << "s" << endl;
		suma += wynik;
	}
	cout << "Srednia: " << suma / 10.0;
}

